Metadata-Version: 2.1
Name: trytond_account_tax_rule_country
Version: 7.0.2
Summary: Tryton module to add countries on tax rules
Home-page: http://www.tryton.org/
Download-URL: http://downloads.tryton.org/7.0/
Author: Tryton
Author-email: foundation@tryton.org
License: GPL-3
Project-URL: Bug Tracker, https://bugs.tryton.org/
Project-URL: Documentation, https://docs.tryton.org/
Project-URL: Forum, https://www.tryton.org/forum
Project-URL: Source Code, https://code.tryton.org/tryton
Keywords: tryton account tax rule country
Classifier: Development Status :: 5 - Production/Stable
Classifier: Environment :: Plugins
Classifier: Framework :: Tryton
Classifier: Intended Audience :: Developers
Classifier: Intended Audience :: Financial and Insurance Industry
Classifier: Intended Audience :: Legal Industry
Classifier: License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
Classifier: Natural Language :: Bulgarian
Classifier: Natural Language :: Catalan
Classifier: Natural Language :: Chinese (Simplified)
Classifier: Natural Language :: Czech
Classifier: Natural Language :: Dutch
Classifier: Natural Language :: English
Classifier: Natural Language :: Finnish
Classifier: Natural Language :: French
Classifier: Natural Language :: German
Classifier: Natural Language :: Hungarian
Classifier: Natural Language :: Indonesian
Classifier: Natural Language :: Italian
Classifier: Natural Language :: Persian
Classifier: Natural Language :: Polish
Classifier: Natural Language :: Portuguese (Brazilian)
Classifier: Natural Language :: Romanian
Classifier: Natural Language :: Russian
Classifier: Natural Language :: Slovenian
Classifier: Natural Language :: Spanish
Classifier: Natural Language :: Turkish
Classifier: Natural Language :: Ukrainian
Classifier: Operating System :: OS Independent
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Programming Language :: Python :: 3.11
Classifier: Programming Language :: Python :: 3.12
Classifier: Programming Language :: Python :: Implementation :: CPython
Classifier: Topic :: Office/Business
Classifier: Topic :: Office/Business :: Financial :: Accounting
Requires-Python: >=3.8
License-File: LICENSE
Requires-Dist: trytond_account<7.1,>=7.0
Requires-Dist: trytond_country<7.1,>=7.0
Requires-Dist: trytond<7.1,>=7.0
Provides-Extra: test
Requires-Dist: trytond_account_invoice<7.1,>=7.0; extra == "test"
Requires-Dist: trytond_sale<7.1,>=7.0; extra == "test"
Requires-Dist: trytond_purchase<7.1,>=7.0; extra == "test"
Requires-Dist: trytond_stock<7.1,>=7.0; extra == "test"

Account Tax Rule Country
########################

The account_tax_rule module extends the tax rule to add origin and destination
countries and subdivisions as criteria.

Tax Rule Line
*************

Four criteria fields are added:

- From Country: The country of origin
- From Subdivision: The subdivision of origin
- To Country: The country of destination
- To Subdivision: The subdivision of destination

The countries are picked from the origin document:

- Sale:

  - The origin country and subdivision come from the address of the warehouse.
  - The destination country and subdivision come from the shipping address.

- Purchase:

  - The origin country and subdivision come from the invoice address.
  - The destination country and subdivision come from the address of the warehouse.

- Stock Consignment:

  - The origin country and subdivision come from the warehouse's address of the
    location or the delivery address for returned customer shipment.
  - The destination country and subdivision come from the warehouse's address
    of the location or the delivery address for customer shipment.
